import django.dispatch


sig_drfbulk_bulk_create_done = django.dispatch.Signal(providing_args=["pks"])
