from rest_framework import viewsets
from rest_framework.decorators import action

from drfbulk.base import BaseViewSetUtils


class DrfBulkViewSet(viewsets.GenericViewSet):

    def get_router_for_drf(self):
        # TODO: Find a better way of getting the router. Remove hardcoded import
        raise NotImplemented

    @action(methods=['post'], detail=False)
    def drfbulk_single_create(self, request):
        router = self.get_router_for_drf()
        return BaseViewSetUtils(request, router).get_or_create(self.queryset.model, self.serializer_class, request.data,
                                                               viewset=self)

    @action(methods=['post'], detail=False)
    def drfbulk_multiple_create(self, request):
        router = self.get_router_for_drf()
        return BaseViewSetUtils(request, router).bulk_get_or_create(self.queryset.model, self.serializer_class,
                                                                    request.data, viewset=self)
