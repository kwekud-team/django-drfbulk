import json
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth import get_user_model


class DrfSingleCreateTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.workspace = Workspace.objects.create(id=1, guid='9d35aa4e-1189-426e-b359-2fef8cfe783a', name='Test Space')
        self.user = get_user_model().objects.create_user(username='test')

        self.default_items = [
            {
                'workspace__guid': self.workspace.guid,
                'remote_content_str': 'posts.post',
                'remote_content_guid': '1d35aa4e-1189-426e-b359-2fef8cfe783a',
                'local_content_str': 'media.picture',
                'section': 'Thumb',
                'defaults': {
                    'extra': {'url': 'https://via.placeholder.com/150'},
                    'created_by': self.user.id,
                    'modified_by': self.user.id,
                }
            },
            {
                'workspace': self.workspace.id,
                'remote_content_str': 'posts.post',
                'remote_content_guid': '1d35aa4e-1189-426e-b359-2fef8cfe783a',
                'local_content_str': 'media.picture',
                'section': 'Thumb',
                'defaults': {
                    'extra': {'url': 'https://via.placeholder.com/151'},
                    'created_by': self.user.id,
                    'modified_by': self.user.id,
                }
            },
            {
                'remote_content_str': 'posts.post',
                'remote_content_guid': '1d35aa4e-1189-426e-b359-2fef8cfe783c',
                'local_content_str': 'media.picture',
                'section': 'Thumb',
                'defaults': {
                    'extra': {'url': 'https://via.placeholder.com/150'},
                    'created_by': self.user.id,
                    'modified_by': self.user.id,
                }
            }
        ]

    def test_get_or_create(self):
        input_list = [
            {
                'INPUT': self.default_items[0],
                'EXPECTED': {'status_code': 201, 'response_type': dict}
            },
            {
                'INPUT': self.default_items[1],
                'EXPECTED': {'status_code': 202, 'response_type': dict}
            },
            {
                'INPUT': self.default_items[2],
                'EXPECTED': {'status_code': 400, 'response_type': dict}
            },
        ]

        for x in input_list:
            resp = self.client.post('/api/v1/processors/remoteaction/_get_or_create/', data=json.dumps(x['INPUT']),
                                    content_type='application/json')
            resp_content = json.loads(resp.content)

            self.assertEqual(resp.status_code, x['EXPECTED']['status_code'])
            self.assertEqual(type(resp_content), x['EXPECTED']['response_type'])

    def test_bulk_get_or_create(self):
        input_list = [
            {
                'INPUT': [
                    self.default_items[0],
                    self.default_items[1],
                    self.default_items[2]
                ],
                'EXPECTED': {'status_codes': [201, 202, 400], 'response_type': list}
            }
        ]

        for x in input_list:
            resp = self.client.post('/api/v1/processors/remoteaction/_bulk_get_or_create/', data=json.dumps(x['INPUT']),
                                    content_type='application/json')
            resp_content = json.loads(resp.content)

            status_codes = [y['status_code'] for y in resp_content]
            self.assertEqual(status_codes, x['EXPECTED']['status_codes'])
            self.assertEqual(type(resp_content), x['EXPECTED']['response_type'])
