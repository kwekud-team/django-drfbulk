from enum import Enum


class DrfBulkK(Enum):
    FIELDS_ALL = '_all'
    FIELDS_META = '_meta'
    FIELDS_UNIQUE = '_unique'
    FIELDS_OPTIONAL = '_optional'
    FIELDS_CHILDREN = '_children'
    FIELDS_APP_MODEL = '_app_model'
    FIELDS_OBJECTS = '_objects'
    FIELDS_PARENT_FIELD_NAME = '_parent_field_name'
