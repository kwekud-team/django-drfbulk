from rest_framework import status
from rest_framework.response import Response
from rest_framework.reverse import reverse

from drfbulk.utils import get_model_from_str
from drfbulk.constants import DrfBulkK
from drfbulk.signals import sig_drfbulk_bulk_create_done


class BaseViewSetUtils:
    SAVE_TYPE_GET = 'GET'
    SAVE_TYPE_CREATE = 'CREATE'
    SAVE_TYPE_UPDATE = 'UPDATE'

    def __init__(self, request, router):
        self.request = request
        self.router = router
        self.key_save_all = 'save%s' % DrfBulkK.FIELDS_ALL.value
        self.key_query_unique = 'query%s' % DrfBulkK.FIELDS_UNIQUE.value
        self.key_save_unique = 'save%s' % DrfBulkK.FIELDS_UNIQUE.value
        self.key_save_optional = 'save%s' % DrfBulkK.FIELDS_OPTIONAL.value
        self.key_value_meta = 'value%s' % DrfBulkK.FIELDS_META.value
        self.key_value_children = 'value%s' % DrfBulkK.FIELDS_CHILDREN.value

    def get_serializer_from_model(self, model_class):
        serializer_class = None

        for y in self.router.registry:
            viewset = y[1]
            # if get_str_from_model(viewset.queryset.model) == get_str_from_model(model_class):
            if viewset.queryset.model == model_class:
                serializer_class = viewset.serializer_class
                break

        return serializer_class

    def prepare_field(self, model_class, serializer_class, field_name, value):
        field = serializer_class().fields.get(field_name)

        if isinstance(value, dict) and DrfBulkK.FIELDS_UNIQUE.value in value:
            child_model_class = field.queryset.model
            child_serializer_class = self.get_serializer_from_model(child_model_class)

            obj = self.get_or_create(child_model_class, child_serializer_class, value, return_raw_response=True)

            if obj:
                obj_id = obj.get('serialized_obj', {}).get('id', 0)
                value = reverse(field.view_name, args=(obj_id,))

        elif hasattr(field, 'queryset'):
            if '/' not in str(value):
                url = reverse(field.view_name, args=(value,))
                value = url

        elif '__' in field_name and value:
            """
            This is for data that references fields in related models.
            eg: entity__position=3
            """
            split_xs = field_name.split('__')
            local_name, all_rel_names, rel_name = split_xs[0], split_xs[:-1], split_xs[-1]
            rel_model_class = model_class
            for x in all_rel_names:
                rel_field = rel_model_class._meta.get_field(x)
                rel_model_class = rel_field.related_model
            obj = rel_model_class.objects.get(**{rel_name: value})
            # obj = model_class.objects.get(**{field_name: value})

            new_field = serializer_class().fields.get(local_name)
            url = reverse(new_field.view_name, args=(obj.pk,))
            value = url
            field_name = local_name

        else:
            value = value

        return field_name, value

    def prepare_data(self, model_class, serializer_class, raw_data):
        request_data_copy = raw_data.copy()
        meta = request_data_copy.pop(DrfBulkK.FIELDS_META.value, {})
        uniques = request_data_copy.pop(DrfBulkK.FIELDS_UNIQUE.value, {})
        optional = request_data_copy.pop(DrfBulkK.FIELDS_OPTIONAL.value, {})
        children = request_data_copy.pop(DrfBulkK.FIELDS_CHILDREN.value, {})

        dt = {
            self.key_save_all: {},
            self.key_query_unique: {},
            self.key_save_unique: {},
            self.key_save_optional: {},
            self.key_value_meta: meta,
            self.key_value_children: children
        }
        for k, v in uniques.items():
            prepared_k, prepared_v = self.prepare_field(model_class, serializer_class, k, v)
            dt[self.key_query_unique][k] = v
            dt[self.key_save_unique][prepared_k] = prepared_v
            dt[self.key_save_all][prepared_k] = prepared_v

        for k, v in optional.items():
            prepared_k, prepared_v = self.prepare_field(model_class, serializer_class, k, v)
            dt[self.key_save_optional][prepared_k] = prepared_v
            dt[self.key_save_all][prepared_k] = prepared_v

        return dt

    def process_children(self, serialized_obj, child_info):
        xs = []
        child_model_class = get_model_from_str(child_info[DrfBulkK.FIELDS_APP_MODEL.value])
        child_serializer_class = self.get_serializer_from_model(child_model_class)
        for child in child_info[DrfBulkK.FIELDS_OBJECTS.value]:
            parent_field = child_info.get(DrfBulkK.FIELDS_PARENT_FIELD_NAME.value, None)
            if parent_field:
                child[DrfBulkK.FIELDS_UNIQUE.value][parent_field] = serialized_obj['pk']
            res = self.get_or_create(child_model_class, child_serializer_class, child, return_raw_response=True)
            xs.append(res)
        return xs

    def get_or_create(self, model_class, serializer_class, data, **kwargs):
        prepared_data = self.prepare_data(model_class, serializer_class, data)

        obj, save_type = None, None

        try:
            obj = model_class.objects.get(**prepared_data[self.key_query_unique])
            save_type = self.SAVE_TYPE_GET

            if prepared_data[self.key_value_meta].get('should_update', True):
                save_type = self.SAVE_TYPE_UPDATE

        except model_class.DoesNotExist:
            save_type = self.SAVE_TYPE_CREATE

        except model_class.MultipleObjectsReturned:
            status_code = status.HTTP_400_BAD_REQUEST
            serialized_obj = {"detail": "Multiple Objects Returned."}
            return Response(serialized_obj, status=status_code)

        if save_type == self.SAVE_TYPE_GET:
            serial_obj = serializer_class(obj, context={'request': self.request})
            serialized_obj = serial_obj.data
            status_code = status.HTTP_200_OK
        else:
            if save_type == self.SAVE_TYPE_UPDATE:
                serial_dt = serializer_class(instance=obj, data=prepared_data[self.key_save_optional], partial=True,
                                             context={'request': self.request})
                status_code = status.HTTP_202_ACCEPTED
            else:
                serial_dt = serializer_class(data=prepared_data[self.key_save_all], context={'request': self.request})
                status_code = status.HTTP_201_CREATED

            if serial_dt.is_valid():
                serial_dt.save()
                status_code = status_code
                serialized_obj = serial_dt.data

            else:
                status_code = status.HTTP_400_BAD_REQUEST
                serialized_obj = serial_dt.errors

        if status_code == status.HTTP_400_BAD_REQUEST:
            print('ERROR: ', model_class, save_type, serialized_obj, prepared_data)

        # children
        child_info = prepared_data[self.key_value_children]
        if child_info and status_code != status.HTTP_400_BAD_REQUEST:
            children_res = self.process_children(serialized_obj, child_info)
            if children_res:
                serialized_obj[DrfBulkK.FIELDS_CHILDREN.value] = children_res

        if kwargs.get('return_serialized_obj'):
            return serialized_obj

        if kwargs.get('return_raw_response'):
            return {
                'serialized_obj': serialized_obj,
                'status_code': status_code
            }

        return Response(serialized_obj, status=status_code)

    def bulk_get_or_create(self, model_class, serializer_class, data, **kwargs):
        xs = []
        pks = []
        status_code = status.HTTP_202_ACCEPTED

        objects = data.get(DrfBulkK.FIELDS_OBJECTS.value, {})
        for x in objects:
            dt = self.get_or_create(model_class, serializer_class, x, return_raw_response=True, **kwargs)
            xs.append(dt)

            if dt['status_code'] != status.HTTP_400_BAD_REQUEST:
                pks.append(str(dt['serialized_obj']['pk']))
            else:
                status_code = dt['status_code']
                break

        sig_drfbulk_bulk_create_done.send(sender=model_class, pks=','.join(pks))

        print("PPPPPPPPP", xs, objects, data)
        return Response(xs, status=status_code)
