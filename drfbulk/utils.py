from django.apps import apps


def get_model_from_str(app_model, raise_exception=False):
    # Get model from string in format 'app_label.model_name'
    app_model = str(app_model)
    if app_model and '.' in app_model:
        app_label, model_name = app_model.split('.')
        try:
            mc = apps.get_model(app_label, model_name)
        except LookupError:
            mc = None
        if not mc and raise_exception:
            raise Exception("App model not found: %s" % app_model)
        return mc


def get_str_from_model(model):
    """ Get model from string in format 'app_label.model_name' """
    app_label = model._meta.app_label
    model_name = model._meta.model_name
    return '%s.%s' % (app_label, model_name)
