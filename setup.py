import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name='drfbulk',
     version='0.1',
     # scripts=['drfbulk'],
     author="KDanso",
     author_email="dev@kkdanso.com",
     description="Add bulk inserts for Django Rest Framework",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://bitbucket.org/kwekud-team/drfbulk",
     packages=setuptools.find_packages(),
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
)